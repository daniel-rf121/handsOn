import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

export const LOCAIS = [
  {
    id: 1,
    nome: 'New York',
    resume: 'Cards can be used to achieve a multitude of designs. We provide many of.',
    text: 'Cards can be used to achieve a multitude of designs. We provide many of the elements to achieve common designs, but sometimes it will be necessary to add custom styles. Adding background images to cards is a perfect example of how adding custom styles can achieve a completely different look.',
    banner: 'assets/imgs/new_york.png',
    pontos: [
      {
          nome: 'Ponto turístico 1'
      },
      {
          nome: 'Ponto turístico 2'
      },
      {
          nome: 'Ponto turístico 3'
      },
      {
          nome: 'Ponto turístico 4'
      },
      {
          nome: 'Ponto turístico 5'
      }
    ]
  },
  {
    id: 2,
    nome: 'Paris',
    resume: 'Cards can be used to achieve a multitude of designs. We provide many of.',
    text: 'Cards can be used to achieve a multitude of designs. We provide many of the elements to achieve common designs, but sometimes it will be necessary to add custom styles. Adding background images to cards is a perfect example of how adding custom styles can achieve a completely different look.',
    banner: 'assets/imgs/paris.png',
    pontos: [
      {
          nome: 'Ponto turístico 1'
      },
      {
          nome: 'Ponto turístico 2'
      },
      {
          nome: 'Ponto turístico 3'
      },
      {
          nome: 'Ponto turístico 4'
      },
      {
          nome: 'Ponto turístico 5'
      }
    ]
  },
  {
    id: 3,
    nome: 'Orlando',
    resume: 'Cards can be used to achieve a multitude of designs. We provide many of.',
    text: 'Cards can be used to achieve a multitude of designs. We provide many of the elements to achieve common designs, but sometimes it will be necessary to add custom styles. Adding background images to cards is a perfect example of how adding custom styles can achieve a completely different look.',
    banner: 'assets/imgs/orlando.png',
    pontos: [
      {
          nome: 'Ponto turístico 1'
      },
      {
          nome: 'Ponto turístico 2'
      },
      {
          nome: 'Ponto turístico 3'
      },
      {
          nome: 'Ponto turístico 4'
      },
      {
          nome: 'Ponto turístico 5'
      }
    ]
  },
  {
    id: 4,
    nome: 'São Francisco',
    resume: 'Cards can be used to achieve a multitude of designs. We provide many of.',
    text: 'Cards can be used to achieve a multitude of designs. We provide many of the elements to achieve common designs, but sometimes it will be necessary to add custom styles. Adding background images to cards is a perfect example of how adding custom styles can achieve a completely different look.',
    banner: 'assets/imgs/sao_francisco.png',
    pontos: [
      {
          nome: 'Ponto turístico 1'
      },
      {
          nome: 'Ponto turístico 2'
      },
      {
          nome: 'Ponto turístico 3'
      },
      {
          nome: 'Ponto turístico 4'
      },
      {
          nome: 'Ponto turístico 5'
      }
    ]
  },
  {
    id: 5,
    nome: 'São Paulo',
    resume: 'Cards can be used to achieve a multitude of designs. We provide many of.',
    text: 'Cards can be used to achieve a multitude of designs. We provide many of the elements to achieve common designs, but sometimes it will be necessary to add custom styles. Adding background images to cards is a perfect example of how adding custom styles can achieve a completely different look.',
    banner: 'assets/imgs/sao_paulo.png',
    pontos: [
      {
          nome: 'Ponto turístico 1'
      },
      {
          nome: 'Ponto turístico 2'
      },
      {
          nome: 'Ponto turístico 3'
      },
      {
          nome: 'Ponto turístico 4'
      },
      {
          nome: 'Ponto turístico 5'
      }
    ]
  },
  {
    id: 6,
    nome: 'Seoul',
    resume: 'Cards can be used to achieve a multitude of designs. We provide many of.',
    text: 'Cards can be used to achieve a multitude of designs. We provide many of the elements to achieve common designs, but sometimes it will be necessary to add custom styles. Adding background images to cards is a perfect example of how adding custom styles can achieve a completely different look.',
    banner: 'assets/imgs/seoul.png',
    pontos: [
      {
          nome: 'Ponto turístico 1'
      },
      {
          nome: 'Ponto turístico 2'
      },
      {
          nome: 'Ponto turístico 3'
      },
      {
          nome: 'Ponto turístico 4'
      },
      {
          nome: 'Ponto turístico 5'
      }
    ]
  },
  {
    id: 7,
    nome: 'Sidney',
    resume: 'Cards can be used to achieve a multitude of designs. We provide many of.',
    text: 'Cards can be used to achieve a multitude of designs. We provide many of the elements to achieve common designs, but sometimes it will be necessary to add custom styles. Adding background images to cards is a perfect example of how adding custom styles can achieve a completely different look.',
    banner: 'assets/imgs/sidney.png',
    pontos: [
      {
          nome: 'Ponto turístico 1'
      },
      {
          nome: 'Ponto turístico 2'
      },
      {
          nome: 'Ponto turístico 3'
      },
      {
          nome: 'Ponto turístico 4'
      },
      {
          nome: 'Ponto turístico 5'
      }
    ]
  }
];

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public locais: Array<any> = LOCAIS;

  constructor(public navCtrl: NavController) {
  }

  detail() {
    console.log('batata');
  }
}
